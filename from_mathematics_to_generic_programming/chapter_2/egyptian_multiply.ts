/**
 * The naive addition n times approach.
 * 1. a * 1 = a
 * 2. a * n = a * (n - 1) + a
 */
export function simpleMultiply(
	n: number,
	a: number,
	self = simpleMultiply,
) {
	if (n === 1) return a
	return self(n - 1, a, self) + a
}

// helpers for egyptian algorithm
export function odd(n: number) {
	return n & 1 // Test the last significant binary bit
}

export function half(n: number) {
	return n >> 1 // single binary right shift
}

/**
 * recursive version of egyptian multiplication.
 */
export function egyptMult1(
	n: number,
	a: number,
	self = egyptMult1,
) {
	if (n === 1) return a
	const r = self(half(n), a + a, self)
	if (odd(n)) {
		return r + a
	} else {
		return r
	}
}

/**
 * tail recursive version of egyptian multiplication,
 * with result accumulation.
 */
export function multiplyAcc0(
	r: number,
	n: number,
	a: number,
	self = multiplyAcc0,
) {
	if (n === 1) return r + a
	if (odd(n)) {
		return self(r + a, half(n), a + a, self)
	} else {
		return self(r, half(n), a + a, self)
	}
}

/**
 * strict tail recursive version of egyptian multiplication,
 * with result accumulation.
 * strict form means: only one recursive call of the function at the end,
 * with original params as inputs
 */
export function multiplyAcc1(
	r: number,
	n: number,
	a: number,
	self = multiplyAcc0,
) {
	if (odd(n)) {
		r = r + a
		if (n === 1) return r
	}
	n = half(n)
	a = a + a
	return self(r, n, a, self)
}

/**
 * Imperative with while loop
 */
export function multiplyWhile(
	r: number,
	n: number,
	a: number,
) {
	while (true) {
		if (odd(n)) {
			r = r + a
			if (n === 1) return r
		}
		n = half(n)
		a = a + a
	}
}

/**
 * super optimized version of multiply
 */
export function multiply(n: number, a: number) {
	// optimize if n = 2^x
	while (!odd(n)) {
		n = half(n)
		a = a + a
	}

	if (n === 1) return a

	// now n >= 3 and odd
	const r = a
	n = half(n - 1)
	a = a + a

	return multiplyWhile(r, n, a)
}
