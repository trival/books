import { getTable } from 'console.table'
import { filterSpyCallArgs, spy } from '../../utils/spy'
import {
	egyptMult1,
	half,
	multiply,
	multiplyAcc0,
	multiplyAcc1,
	multiplyWhile,
	odd,
	simpleMultiply,
} from './egyptian_multiply'

const multTests = [
	[3, 4, 12],
	[4, 3, 12],
	[7, 5, 35],
	[4, 2, 8],
	[1, 5, 5],
]

describe('egyptian_multiply', () => {
	describe('half', () => {
		it('tells if a number is odd', () => {
			expect(half(24)).toBe(12)
			expect(half(4)).toBe(2)
			expect(half(5)).toBe(2)
			expect(half(205)).toBe(102)
		})
	})

	describe('odd', () => {
		it('tells if a number is odd', () => {
			expect(odd(24)).toBe(0)
			expect(odd(4)).toBe(0)
			expect(odd(5)).toBe(1)
			expect(odd(205)).toBe(1)
		})
	})

	describe('simpleMultiply', () => {
		it('multiplies numbers', () => {
			multTests.forEach(([a, b, r]) =>
				expect(simpleMultiply(a, b)).toBe(r),
			)
		})

		it('runs n times', () => {
			const m = spy(simpleMultiply)
			expect(m(4, 3, m)).toBe(12)
			expect(m.calls.length).toBe(4)
			m.clearCalls()
			expect(m(7, 3, m)).toBe(21)
			expect(m.calls.length).toBe(7)
			m.clearCalls()
			expect(m(21, 3, m)).toBe(63)
			expect(m.calls.length).toBe(21)
		})
	})

	describe('egyptMult1', () => {
		it('multiplies numbers', () => {
			multTests.forEach(([a, b, r]) =>
				expect(egyptMult1(a, b)).toBe(r),
			)
		})

		it('runs O(log2(n)) times', () => {
			const m = spy(egyptMult1)
			expect(m(4, 3, m)).toBe(12)
			expect(m.calls.length).toBe(3)

			m.clearCalls()
			expect(m(7, 3, m)).toBe(21)
			expect(m.calls.length).toBe(3)

			m.clearCalls()
			expect(m(21, 3, m)).toBe(63)
			expect(m.calls.length).toBe(5)

			m.clearCalls()
			expect(m(201, 3, m)).toBe(603)
			expect(m.calls.length).toBe(8)
			console.log(
				getTable(
					'egyptMult1',
					filterSpyCallArgs(
						m.calls,
						a => typeof a === 'number',
					),
				),
			)
		})
	})

	describe('multiplyAcc0', () => {
		it('multiplies numbers', () => {
			multTests.forEach(([a, b, r]) =>
				expect(multiplyAcc0(0, a, b)).toBe(r),
			)
		})

		it('runs O(log2(n)) times', () => {
			const m = spy(multiplyAcc0)
			expect(m(0, 201, 3, m)).toBe(603)
			expect(m.calls.length).toBe(8)
			console.log(
				getTable(
					'multiplyAcc0',
					filterSpyCallArgs(
						m.calls,
						a => typeof a === 'number',
					),
				),
			)
		})
	})

	describe('multiplyAcc1', () => {
		it('multiplies numbers', () => {
			multTests.forEach(([a, b, r]) =>
				expect(multiplyAcc1(0, a, b)).toBe(r),
			)
		})

		it('runs O(log2(n)) times', () => {
			const m = spy(multiplyAcc1)
			expect(m(0, 201, 3, m)).toBe(603)
			expect(m.calls.length).toBe(8)
			console.log(
				getTable(
					'multiplyAcc1',
					filterSpyCallArgs(
						m.calls,
						a => typeof a === 'number',
					),
				),
			)
		})
	})

	describe('multiplyWhile', () => {
		it('multiplies numbers', () => {
			multTests.forEach(([a, b, r]) =>
				expect(multiplyWhile(0, a, b)).toBe(r),
			)
		})
	})

	describe('multiply', () => {
		it('multiplies numbers', () => {
			multTests.forEach(([a, b, r]) =>
				expect(multiply(a, b)).toBe(r),
			)
		})
	})
})
