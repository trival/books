// first encounter of gcm in the book
export function gcm(a: number, b: number) {
	if (a < b) return gcm(a, b - a)
	if (a > b) return gcm(b, a - b)
	return a
}
