import { gcm } from './gcm1'

describe('greates common measure', () => {
	it('calculates gcm', () => {
		expect(gcm(196, 42)).toBe(14)
		expect(gcm(197, 42)).toBe(1)
		expect(gcm(20, 40)).toBe(20)
		expect(gcm(19, 41.25)).toBe(0.25)
	})
})
