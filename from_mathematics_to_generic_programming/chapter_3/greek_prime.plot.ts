import { getTable } from 'console.table'
import * as plotlySetup from 'plotly'
import { zip } from '../../libs/tvs-libs/lib/utils/sequence'
import { primes2 } from './greek_prime_sift'

const plotly = plotlySetup(
	process.env.PLOTLY_USERNAME,
	process.env.PLOTLY_API_KEY,
)

const MAX_INT = 10000000
const INCREMENT = 1000
const primes = primes2(MAX_INT)
const numbers: number[] = []
const primeCount: number[] = []

primes.unshift(1)

let primeIdx = 0
for (let i = INCREMENT; i <= MAX_INT; i += INCREMENT) {
	numbers.push(i)
	while (primes[primeIdx + 1] <= i) {
		primeIdx++
	}
	primeCount.push(primeIdx + 1)
}

// console.log(primes)
// console.log(
// 	getTable(
// 		['number', 'primCount'],
// 		zip((a, b) => [a, b], numbers, primeCount),
// 	),
// )

const data = [{ x: numbers, y: primeCount, type: 'bar' }]
const layout = {
	fileopt: 'overwrite',
	filename: 'primes-plot',
}

plotly.plot(data, layout, (err, msg) => {
	if (err) return console.log(err)
	console.log(msg)
})
