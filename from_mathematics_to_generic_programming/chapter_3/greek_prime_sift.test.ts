import { primes1, primes2 } from './greek_prime_sift'
import { primesUntillThousand } from './primes_test_data'

describe('prime sift', () => {
	it('primes1 sifts primes', () => {
		expect(primes1(1000)).toEqual(primesUntillThousand)
	})

	it('primes2 sifts primes', () => {
		expect(primes2(1000)).toEqual(primesUntillThousand)
	})
})
