import { Sequence } from '../../libs/tvs-libs/lib/utils/sequence'

const idxToVal = (idx: number) => idx * 2 + 3
const valToIdx = (val: number) => (val - 3) / 2

function setupPrimes(
	sift: (arr: Sequence<boolean>) => void,
) {
	const rawPrimes = setupRaw(sift)
	return function primes(maxInt: number) {
		const primes = rawPrimes(maxInt).reduce(
			(primes, bool, idx) => {
				if (bool) {
					primes.push(idxToVal(idx))
				}
				return primes
			},
			[2],
		)

		return primes
	}
}

function setupRaw(sift: (arr: Sequence<boolean>) => void) {
	return function primes(maxInt: number) {
		const lastIdx = Math.floor(valToIdx(maxInt))
		const arr: boolean[] = Array(lastIdx + 1).fill(true)

		sift(arr)

		return arr
	}
}

// self made
function sift1(arr: Sequence<boolean>) {
	const lastIdx = arr.length - 1
	const lastTestableIndex = Math.floor(
		valToIdx(Math.sqrt(idxToVal(lastIdx))),
	)
	for (let i = 0; i <= lastTestableIndex; i++) {
		if (arr[i]) {
			const val = idxToVal(i)
			const firstIdx = valToIdx(val * val)
			for (let j = firstIdx; j <= lastIdx; j += val) {
				arr[j] = false
			}
		}
	}
}

// optimized with suggestions from the book
function sift2(arr: Sequence<boolean>) {
	const lastIdx = arr.length - 1
	let val = 3
	let firstIdx = 3
	let i = 0
	while (firstIdx <= lastIdx) {
		if (arr[i]) {
			for (let j = firstIdx; j <= lastIdx; j += val) {
				arr[j] = false
			}
		}
		firstIdx += val
		val += 2
		firstIdx += val
		i++
	}
}

export const primes1 = setupPrimes(sift1)
export const primes2 = setupPrimes(sift2)
