import { half } from '../chapter_2/egyptian_multiply'

// strict tail recursive gcm
export function gcmRec(a: number, b: number) {
	if (a === b) {
		return a
	}
	if (a < b) b -= a
	if (a > b) a -= b
	return gcmRec(a, b)
}

export function gcmWhile(a: number, b: number) {
	while (a !== b) {
		if (a < b) b -= a
		if (a > b) a -= b
	}
	return a
}

export function gcmSimpleRemainder(a: number, b: number) {
	while (a !== b) {
		while (a > b) a -= b // Remainder of a / b
		if (a < b) b -= a
	}
	return a
}

export function gcmRemainder(
	a: number,
	b: number,
	remainder: (a: number, b: number) => number,
) {
	while (b !== 0) {
		a = remainder(a, b)
		// swap a and b
		const tmp = b
		b = a
		a = tmp
	}
	return a
}

export function simpleRemainder(a: number, b: number) {
	while (a >= b) a -= b
	return a
}

/**
 * Calculates the remainder of integer division.
 * Uses doubling and halving like the egyption multiplication.
 */
export function remainderRecursive(
	a: number,
	b: number,
	self = remainderRecursive,
) {
	if (a < b) return a
	if (a - b < b) return a - b
	a = self(a, b + b, self)
	if (a < b) return a
	return a - b
}

function largestDoubling(a: number, b: number) {
	while (a - b >= b) b += b
	return b
}

export function remainderImperative(a: number, b: number) {
	if (a < b) return a
	let c = largestDoubling(a, b)
	a -= c
	while (c != b) {
		c = c / 2
		if (c <= a) a -= c
	}
	return a
}

export function egiptianDivision(a: number, b: number) {
	if (a < b) return { quotient: 0, remainder: a }
	let c = largestDoubling(a, b)
	let n = 1
	a -= c
	while (c != b) {
		c = c / 2
		n += n
		if (c <= a) {
			a -= c
			n++
		}
	}
	return { quotient: n, remainder: a }
}

// just use buildin reminder
export function gcd(a: number, b: number) {
	while (b !== 0) {
		a = a % b
		// swap a and b
		const tmp = b
		b = a
		a = tmp
	}
	return a
}
