import { getTable } from 'console.table'
import { filterSpyCallArgs, spy } from '../../utils/spy'
import {
	gcmRec,
	gcmRemainder,
	gcmSimpleRemainder,
	gcmWhile,
	remainderRecursive,
	simpleRemainder,
	remainderImperative,
	gcd,
	egiptianDivision,
} from './gcm'

function testGcm(gcm: (a: number, b: number) => number) {
	expect(gcm(196, 42)).toBe(14)
	expect(gcm(197, 42)).toBe(1)
	expect(gcm(20, 40)).toBe(20)
	expect(gcm(16, 152)).toBe(8)
	// expect(gcm(19, 41.25)).toBe(0.25)
}

describe('greatest common measure', () => {
	describe('gcm', () => {
		it('gcm strict tail recursive', () => {
			testGcm(gcmRec)
		})

		it('gcm while', () => {
			testGcm(gcmWhile)
		})

		it('simple remainder', () => {
			testGcm(gcmSimpleRemainder)
		})

		it('works with different reminder functions', () => {
			const makeTest = remainder => (a, b) =>
				gcmRemainder(a, b, remainder)

			testGcm(makeTest(simpleRemainder))
			testGcm(makeTest(remainderRecursive))
			testGcm(makeTest(remainderImperative))
		})

		it('works with modulo', () => {
			testGcm(gcd)
		})
	})

	describe('reminder', () => {
		const testArgs = [
			[5, 3],
			[3, 5],
			[65, 12],
			[12, 4],
			[8, 2],
		]
		const testResults = [2, 3, 5, 0, 0]
		function testRemainder(remainder) {
			return testArgs.map(args => remainder(...args))
		}

		it('works', () => {
			expect(testRemainder(simpleRemainder)).toEqual(
				testResults,
			)
			expect(testRemainder(remainderRecursive)).toEqual(
				testResults,
			)
			expect(testRemainder(remainderImperative)).toEqual(
				testResults,
			)
		})

		it('recurses', () => {
			const r = spy(remainderRecursive)
			expect(r(45, 6, r)).toBe(3)
			console.log(
				getTable(
					'remainderRecursive(45, 6)',
					filterSpyCallArgs(
						r.calls,
						a => typeof a === 'number',
					),
				),
			)
			r.clearCalls()
			expect(r(165, 8, r)).toBe(5)
			console.log(
				getTable(
					'remainderRecursive(165, 8)',
					filterSpyCallArgs(
						r.calls,
						a => typeof a === 'number',
					),
				),
			)
		})
	})

	describe('egyptian division', () => {
		it('works', () => {
			const makeQotient = (q, r) => ({
				quotient: q,
				remainder: r,
			})
			expect(egiptianDivision(8, 2)).toEqual(
				makeQotient(4, 0),
			)
			expect(egiptianDivision(12, 5)).toEqual(
				makeQotient(2, 2),
			)
			expect(egiptianDivision(45, 6)).toEqual(
				makeQotient(7, 3),
			)
		})
	})
})
