import { spy } from './spy'

describe('spy', () => {
	it('spies on functions', () => {
		const f = (a, b) => a + b
		const fSpy = spy(f)
		expect(fSpy.fn).toBe(f)
		expect(fSpy(2, 3)).toBe(5)
		expect(fSpy.calls).toEqual([
			{ args: [2, 3], result: 5, resultOrder: 0 }
		])
		expect(fSpy(3, 4)).toBe(7)
		const calls1 = fSpy.calls
		expect(calls1).toEqual([
			{ args: [2, 3], result: 5, resultOrder: 0 },
			{ args: [3, 4], result: 7, resultOrder: 1 }
		])

		fSpy.clearCalls()
		expect(fSpy.calls).toEqual([])
		expect(fSpy(4, 5)).toBe(9)
		expect(fSpy.calls).toEqual([
			{ args: [4, 5], result: 9, resultOrder: 0 }
		])

		expect(calls1).toEqual([
			{ args: [2, 3], result: 5, resultOrder: 0 },
			{ args: [3, 4], result: 7, resultOrder: 1 }
		])
	})
})
