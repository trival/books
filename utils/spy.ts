interface SpyArgs {
	args: any[]
	result: any
	resultOrder: number
}

export function spy(fn: Function) {
	let resolved = 0
	function newSpy(...args: any[]) {
		const data = { args } as SpyArgs
		newSpy.calls.push(data)
		const result = fn(...args)
		data.result = result
		data.resultOrder = resolved++
		return result
	}

	newSpy.calls = [] as SpyArgs[]
	newSpy.fn = fn
	newSpy.clearCalls = () => ((newSpy.calls = []), (resolved = 0))
	return newSpy
}

export function filterSpyCallArgs(calls: SpyArgs[], fn) {
	return calls.map(c => ({
		...c,
		args: c.args.filter(fn)
	}))
}
