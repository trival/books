export function createStatementData(invoice, plays) {
	const result: any = {}
	result.customer = invoice.customer
	result.performances = invoice.performances.map(
		enrichPerformances,
	)
	result.totalAmount = totalAmount(result.performances)
	result.totalVolumeCredits = totalVolumeCredits(
		result.performances,
	)
	return result

	function enrichPerformances(aPerformance) {
		const calculator = createPerformanceCalculator(
			aPerformance,
			playFor(aPerformance),
		)
		const result = { ...aPerformance }
		result.play = calculator.play
		result.amount = calculator.amount
		result.volumeCredits = calculator.volumeCredits
		return result
	}

	function playFor(aPerformance) {
		return plays[aPerformance.playID]
	}

	function totalAmount(performances) {
		return performances.map(p => p.amount).reduce(add, 0)
	}

	function totalVolumeCredits(performances) {
		return performances
			.map(p => p.volumeCredits)
			.reduce(add, 0)
	}

	function add(a, b) {
		return a + b
	}
}

class PerformanceCalculator {
	performance
	play

	constructor(aPerformance, aPlay) {
		this.performance = aPerformance
		this.play = aPlay
	}

	get amount(): number {
		throw new Error('subclass responsibility')
	}

	get volumeCredits() {
		let result = Math.max(this.performance.audience - 30, 0)
		if ('comedy' === this.play.type) {
			result += Math.floor(this.performance.audience / 5)
		}
		return result
	}
}

class TragedyCalculator extends PerformanceCalculator {
	get amount() {
		let result = 40000
		if (this.performance.audience > 30) {
			result += 1000 * (this.performance.audience - 30)
		}
		return result
	}
}
class ComedyCalculator extends PerformanceCalculator {
	get amount() {
		let result = 30000
		if (this.performance.audience > 20) {
			result +=
				10000 + 500 * (this.performance.audience - 20)
		}
		result += 300 * this.performance.audience
		return result
	}
}

function createPerformanceCalculator(aPerformance, aPlay) {
	switch (aPlay.type) {
		case 'tragedy':
			return new TragedyCalculator(aPerformance, aPlay)
		case 'comedy':
			return new ComedyCalculator(aPerformance, aPlay)
		default:
			throw new Error('unknown type: ' + aPlay.type)
	}
}
