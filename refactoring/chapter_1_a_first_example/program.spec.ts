import { statement as originalStatement } from './original_program'
import { htmlStatement, statement } from './program'
import { createStatementData } from './statementData'

describe('refactoring-chapter-1', () => {
	describe('original-program', () => {
		it('renders correctly', () => {
			const plays = require('./plays')
			const invoices = require('./invoices.json')

			expect(originalStatement(invoices[0], plays))
				.toMatchInlineSnapshot(`
			"Statement for BigCo
			  Hamlet: $650.00 (55 seats)
			  As You Like It: $580.00 (35 seats)
			  Othello: $500.00 (40 seats)
			Amount owed is $1,730.00
			You earned 47 credits
			"
		`)
		})
	})

	describe('program', () => {
		it('renders correctly', () => {
			const plays = require('./plays')
			const invoices = require('./invoices.json')

			expect(statement(invoices[0], plays))
				.toMatchInlineSnapshot(`
			"Statement for BigCo
			  Hamlet: $650.00 (55 seats)
			  As You Like It: $580.00 (35 seats)
			  Othello: $500.00 (40 seats)
			Amount owed is $1,730.00
			You earned 47 credits
			"
		`)
		})

		it('renders html', () => {
			const plays = require('./plays')
			const invoices = require('./invoices.json')

			expect(htmlStatement(invoices[0], plays))
				.toMatchInlineSnapshot(`
			"<h1>Statement for BigCo</h1>
			<table>
			<tr><th>play</th><th>seats</th><th>cost</th></tr>  <tr><td>Hamlet</td><td>55</td><td>$650.00</td></tr>
			  <tr><td>As You Like It</td><td>35</td><td>$580.00</td></tr>
			  <tr><td>Othello</td><td>40</td><td>$500.00</td></tr>
			</table>
			<p>Amount owed is <em>$1,730.00</em></p>
			<p>You earned <em>47</em> credits</p>
			"
		`)
		})
	})

	describe('statement data', () => {
		it('produces rendering data', () => {
			const plays = require('./plays')
			const invoices = require('./invoices.json')
			expect(createStatementData(invoices[0], plays))
				.toMatchInlineSnapshot(`
			Object {
			  "customer": "BigCo",
			  "performances": Array [
			    Object {
			      "amount": 65000,
			      "audience": 55,
			      "play": Object {
			        "name": "Hamlet",
			        "type": "tragedy",
			      },
			      "playID": "hamlet",
			      "volumeCredits": 25,
			    },
			    Object {
			      "amount": 58000,
			      "audience": 35,
			      "play": Object {
			        "name": "As You Like It",
			        "type": "comedy",
			      },
			      "playID": "as-like",
			      "volumeCredits": 12,
			    },
			    Object {
			      "amount": 50000,
			      "audience": 40,
			      "play": Object {
			        "name": "Othello",
			        "type": "tragedy",
			      },
			      "playID": "othello",
			      "volumeCredits": 10,
			    },
			  ],
			  "totalAmount": 173000,
			  "totalVolumeCredits": 47,
			}
		`)
		})
	})
})
