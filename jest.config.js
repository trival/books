module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	testPathIgnorePatterns: ["<rootDir>/libs/", "<rootDir>/node_modules/"]
};
